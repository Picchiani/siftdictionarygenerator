#include "siftDictionaryGenerator.h"
#include "controller/JanssonDatasetManager.h"
#include "controller/JanssonManager.h"
#include "controller/SIFTManager.h"
#include <unordered_map>
#include <chrono>
#include <ctime>

using namespace std;
using namespace cv;

/// Contiene i dati di tutte le immagini
std::unordered_map<int, model::ImageData> imagesData;
/// Contiene tutti i dati delle categorie del dataset di train
std::unordered_map<int, model::InstancesForCategory> instancesForCategories;

/**
 * Crea la lista di immagini da cui poi si costruirà il dizionario
 */
std::set<int> createImageList(std::unordered_map<int, model::InstancesForCategory> *instancesForCategories) {
	int image_id;
	std::set<int> image_ids;

	/// Variabile di appoggio
	model::InstancesForCategory instancesForCategory;

	/// Per ogni categoria prendo la lista delle immagini
	for(auto it : (*instancesForCategories)) {

		std::unordered_map<int, std::vector<int>>* instancesForImages;
		instancesForCategory = it.second;
		instancesForImages = instancesForCategory.getInstancesForImages();

		/// Ogni immagine la inserisco nel vettore da ritornare se non è già presente in esso
		for(auto it2 : (*instancesForImages)) {
			image_id = it2.first;

			if(!image_ids.empty()) {

				/// Controllo se è già presente l'elemento, nel caso vado avanti
				if( image_ids.find(image_id) != image_ids.end() ) {
					continue;
				} else {
					image_ids.insert(image_id);
				}
			} else {
				image_ids.insert(image_id);
			}
		}
	}

	return image_ids;
}
/*
 * ./siftDictionaryGenerator
 */
int main(int argc, char **argv) {
	/// Variabili per tenere traccia del tempo impiegato e sua scrittura su file
	ofstream myfile;
	std::chrono::time_point<std::chrono::system_clock> start, end;
	std::chrono::duration<double> elapsed_seconds;
    std::time_t end_time;

	/// Path delle annotazioni e delle immagini di microsoftCOCO
	std::string annotationCOCOPath = rootCOCOPath + COCOAnnotationsPath;
	std::string imageCOCOPath = rootCOCOPath + COCOImagePath;

	/// Inizliazzo il controllore per leggere i dati delle immagini dalle annotazioni di COCO e li leggo
	controller::JanssonManager janssonManager = controller::JanssonManager();
	janssonManager.parseInstancesFile(&imagesData, annotationCOCOPath, type);

	/// Inizializzo il controllore per leggere i dati dal train del dataset e li leggo
	controller::JanssonDatasetManager janssonDatasetManager = controller::JanssonDatasetManager();
	janssonDatasetManager.buildInstancesForCategories(&instancesForCategories, datasetInstancePath, type);

	/// Dalla unordered_map instancesForCategories mi prendo la lista delle immagini
	std::set<int> image_ids = createImageList(&instancesForCategories);

	/// Controller per sift
	controller::SIFTManager siftManager = controller::SIFTManager();
	/// Imposto il path da dove devono essere caricate le immagini
	siftManager.setPathDataset(imageCOCOPath +type +"2014/");
	/// Imposto il path dove devono essere salvati i dizionari
	siftManager.setSaveDictionaryPath(saveDictionaryPath);

	/// Parte di codice per la costruzione del dizionario Bag of SIFT
	/// Estraggo le feature SIFT non clusterizzate
	start = std::chrono::system_clock::now();
	siftManager.extractUnclusteredSiftFeatures(&imagesData, image_ids);
	end = std::chrono::system_clock::now();

	elapsed_seconds = end-start;
    end_time = std::chrono::system_clock::to_time_t(end);

	myfile.open("createDictionaryTime.txt");
	myfile << "Extract SIFT feature unclustered from " << image_ids.size() << " images. Finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";
    myfile.close();

	/// Clusterizzo le feature
	for(const auto& size : dictSize) {
		printf("Creating bag of Sift dictionary for %d visual word...\n", size);

		start = std::chrono::system_clock::now();
		siftManager.createBagOfSift(size);
		end = std::chrono::system_clock::now();

		elapsed_seconds = end-start;
	    end_time = std::chrono::system_clock::to_time_t(end);
	    //std::cout << "finished computation at " << std::ctime(&end_time)
	    //          << "elapsed time: " << elapsed_seconds.count() << "s\n";

	    myfile.open("createDictionaryTime.txt", ios::app);
	    myfile << "Creating bag of Sift dictionary for " << dictSize << " visual word...\n finished computation at " << std::ctime(&end_time)
          	  << "elapsed time: " << elapsed_seconds.count() << "s\n";
	    myfile.close();
	}

}

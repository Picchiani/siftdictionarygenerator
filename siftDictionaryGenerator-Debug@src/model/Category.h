/*
 * Category.h
 *
 *  Created on: Nov 19, 2014
 *      Author: coman
 */

#ifndef SRC_MODEL_CATEGORY_H_
#define SRC_MODEL_CATEGORY_H_

#include <string>

namespace model {

class Category {
public:
	Category();
	Category(int category_id, const std::string& category_label);
	virtual ~Category();

	int getCategoryId() const {
		return category_id;
	}

	void setCategoryId(int categoryId) {
		category_id = categoryId;
	}

	const std::string& getCategoryLabel() const {
		return category_label;
	}

	void setCategoryLabel(const std::string& categoryLabel) {
		category_label = categoryLabel;
	}

	void printCategory() {
		printf("Id: %d Label: %s\n", category_id, category_label.c_str());
	}

private:
	int category_id;
	std::string category_label;
};

} /* namespace model */

#endif /* SRC_MODEL_CATEGORY_H_ */

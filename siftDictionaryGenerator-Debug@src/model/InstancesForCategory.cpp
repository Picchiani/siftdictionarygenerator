/*
 * InstancesForCategory.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: coman
 */

#include "InstancesForCategory.h"

namespace model {

InstancesForCategory::InstancesForCategory() {
	// TODO Auto-generated constructor stub

}

InstancesForCategory::InstancesForCategory(int category_id, std::string category_label) {
	category = model::Category(category_id, category_label);
}


InstancesForCategory::~InstancesForCategory() {
	// TODO Auto-generated destructor stub
}

void InstancesForCategory::addInstanceForImage(int image_id, int instance_id) {
	if(instancesForImages.count(image_id)>0) {
		(instancesForImages.at(image_id)).push_back(instance_id);
	} else {
		std::vector<int> instanceIds = std::vector<int>();
		instanceIds.push_back(instance_id);
		std::pair<int, std::vector<int>> data(image_id, instanceIds);
		instancesForImages.insert(data);
	}
}

std::unordered_map<int, std::vector<int>>* InstancesForCategory::getInstancesForImages() {
	return &instancesForImages;
}

std::string InstancesForCategory::getCategoryLabel() {
	return category.getCategoryLabel();
}

void InstancesForCategory::printInstancesForCategory() {
	category.printCategory();

	for(auto it : instancesForImages) {
		printf("Image_id: %d\n [\n", it.first);

		std::vector<int> instances = it.second;
		for(int instance_id : instances) {
			printf("instance_id: %d\n", instance_id);
		}

		printf("]\n");
	}
}

} /* namespace model */

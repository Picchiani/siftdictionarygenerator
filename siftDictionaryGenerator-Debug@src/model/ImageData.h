/*
 * ImageData.hpp
 *
 * Classe che contiene li informazioni delle diverse immagini del dataset
 *
 *  Created on: Oct 8, 2014
 *      Author: Andrea Picchiani
 */

#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <string>
#include <opencv2/opencv.hpp>
#include <vector>
#include <unordered_map>
//#include "Instance.h"
//#include "Sentence.h"


/*
 *  images[{
 *  	"id" : int,
 *  	"width" : int,
 *  	"height" : int,
 *  	"file_path" : str,
 *  	"file_name" : str,
 *  	"license" : int,
 *  	"url" : str,
 *  	"date_downloaded" : datetime,
 *  }]
 *
 */

namespace model {

class ImageData
{

public:
	ImageData();

	ImageData(int id, const std::string& file_path, const std::string& file_name) :
		id(id),
		file_path(file_path),
		file_name(file_name)
	{

	};

	const std::string& getFileName() const {
		return file_name;
	}

	const std::string& getFilePath() const {
		return file_path;
	}

	int getId() const {
		return id;
	}

	//void printData(void);

	//void addInstance(int instance_id, Instance instance);
	//Instance getInstance(int instance_id);

	//void addSentence(int sentence_id, Sentence sentence);

	//std::vector<int> getSentenceIDs();

	//void setParseTreeForSentence(int sentence_id, std::vector<model::ParseTreeNode> parseTree);

	//std::unordered_map<int, Sentence> getSentences() const {
	//	return sentences;
	//}

	//std::unordered_map<int, Instance> getInstances() const {
	//	return instances;
	//}

private:
	int id;
	std::string file_path;
	std::string file_name;

	//std::unordered_map<int, Sentence> sentences;
	//std::unordered_map<int, Instance> instances;
};

}

#endif /* IMAGE_HPP_ */

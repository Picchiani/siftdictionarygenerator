/*
 * InstancesForCategory.h
 *
 *  Created on: Nov 18, 2014
 *      Author: Andrea Picchiani
 */

#ifndef SRC_MODEL_INSTANCESFORCATEGORY_H_
#define SRC_MODEL_INSTANCESFORCATEGORY_H_

#include "../model/Category.h"
#include <string>
#include <vector>
#include <unordered_map>

namespace model {

class InstancesForCategory {
public:
	InstancesForCategory();
	InstancesForCategory(int category_id, std::string category_label);
	virtual ~InstancesForCategory();

	void addInstanceForImage(int image_id, int instance_id);
	std::unordered_map<int, std::vector<int>>* getInstancesForImages();
	std::string getCategoryLabel();
	void printInstancesForCategory();

private:
	model::Category category;

	// < image_id, vector<instances_id>>
	std::unordered_map<int, std::vector<int>> instancesForImages;
};

} /* namespace model */

#endif /* SRC_MODEL_INSTANCESFORCATEGORY_H_ */

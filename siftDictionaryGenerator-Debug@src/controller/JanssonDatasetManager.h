/*
 * JanssonDatasetManager.h
 *
 * Classe che si occupa di gestire i json del dataset delle instance
 *
 *  Created on: Nov 20, 2014
 *      Author: Andrea Picchiani
 */

#ifndef CONTROLLER_JANSSONDATASETMANAGER_H_
#define CONTROLLER_JANSSONDATASETMANAGER_H_


#include "../model/InstancesForCategory.h"
#include <jansson.h>
#include <string>
#include <unordered_map>

namespace controller {

class JanssonDatasetManager {
public:
	JanssonDatasetManager();
	virtual ~JanssonDatasetManager();

	void buildInstancesForCategories(std::unordered_map<int, model::InstancesForCategory> *instancesForCategories, std::string datasetPath, std::string type);

private:

};

} /* namespace controller */

#endif /* CONTROLLER_JANSSONDATASETMANAGER_H_ */

/*
 * SIFTManager.h
 *
 *  Created on: Dec 15, 2014
 *      Author: Andrea Picchiani
 */

#ifndef CONTROLLER_SIFTMANAGER_H_
#define CONTROLLER_SIFTMANAGER_H_

#include "../model/InstancesForCategory.h"
#include "../model/ImageData.h"
#include <opencv2/nonfree/features2d.hpp>
#include <unordered_map>

namespace controller {

class SIFTManager {

public:
	SIFTManager();
	virtual ~SIFTManager();

	//std::set<int> createImageList(std::unordered_map<int, model::InstancesForCategory> *instancesForCategories);
//	void createBagOfSiftDictionary(std::unordered_map<int, model::ImageData> *imagesData, std::unordered_map<int, model::InstancesForCategory> *instancesForCategories, int dictionary_size);

	void extractUnclusteredSiftFeatures(std::unordered_map<int, model::ImageData> *imagesData,std::set<int> listOfImage);
	void createBagOfSift(int dictSize);

	/**
	 * Serve per impostare il path da dove vengono lette le immagini del dataset Microsoft COCO
	 */
	void setPathDataset(const std::string& pathDataset);
	/**
	 * Serve per impostare il path dove devono essere salvati i dizionari creati
	 */
	void setSaveDictionaryPath(const std::string& saveDictionaryPath);

private:
	/// Root path di dove si trovano le immagini del dataset MicrosoftCOCO
	std::string path_dataset;
	/// Path dove devono essere salvati i dizionari
	std::string saveDictionaryPath;

	/// Immagazzina tutti i descrittori che sono stati estratti dalle immagini.
	cv::Mat featuresUnclustered;

	//cv::Mat dictionary;
	/// Sift feature point extractor
	//cv::Ptr<cv::FeatureDetector> detector;
	/// BoF (or BoW) descriptor extractor
	//cv::BOWImgDescriptorExtractor bowDE;

};

} /* namespace controller */

#endif /* CONTROLLER_SIFTMANAGER_H_ */

/*
 * JanssonDatasetManager.cpp
 *
 * Classe che si occupa di gestire i json del dataset delle instance
 *
 *  Created on: Nov 20, 2014
 *      Author: Andrea Picchiani
 */

#include "JanssonDatasetManager.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

namespace controller {

JanssonDatasetManager::JanssonDatasetManager() {
	// TODO Auto-generated constructor stub

}

JanssonDatasetManager::~JanssonDatasetManager() {
	// TODO Auto-generated destructor stub
}

/**
 * Funzione che si occupa di suddividere la stringa in ingresso mettendo in elements i seguenti elementi:
 * 	0	category_id
 * 	1	category_label
 * @param filepath stringa da suddividere
 * @param elems vettore che conterrà gli elementi suddivisi
 */
std::vector<std::string> &split(const std::string &filepath, std::vector<std::string> &elems) {
	std::stringstream ss(filepath);
	std::string item;
	std::string item2;
	while (std::getline(ss, item, '.')) {
		std::stringstream ss2(item);
		while (std::getline(ss2, item2, '_')) {
			elems.push_back(item2);
		}
	}
}

/**
 * Prende i dati per ogni categoria dai file json contenuti nella directory
 *
 * @param instancesForCategories unordered_map che conterrà i dati estratti dai file json
 */
void JanssonDatasetManager::buildInstancesForCategories(std::unordered_map<int, model::InstancesForCategory> *instancesForCategories, std::string datasetPath, std::string type) {
	//std::string dir_path = "/media/coman/Tesi/datasetClassificatore/";
	std::string dir_path = datasetPath;
	dir_path = dir_path +type +"/";

	printf("Read dataset %s from %s\n", type.c_str(), dir_path.c_str());

	std::ifstream fin;
	std::string filepath;
	DIR *dp;
	char * pch;
	struct dirent *dirp;
	struct stat filestat;

	dp = opendir( dir_path.c_str() );

	/// per ogni file nella directory carico i dati
	while ((dirp = readdir( dp ))) {
		// Oggetto per contenere i dati del dato file
		model::InstancesForCategory instancesForCategory;
		std::string category_id, category_label;

		filepath = dirp->d_name;

		// If the file is a directory (or is in some way invalid) we'll skip it
		// if (stat( filepath.c_str(), &filestat )) continue;
		/// Se il file è una directory lo saltiamo
		if(S_ISDIR( filestat.st_mode )) continue;
		if(!filepath.compare(".")) continue;
		if(!filepath.compare("..")) continue;

		//printf("[buildInstancesFotCategories] filepath %s\n", filepath.c_str());

		/// Divido il nome del file per prendere category_id e category_label
		std::vector<std::string> elems;
		split(filepath, elems);
		category_id = std::string(elems.at(0));
		category_label = std::string(elems.at(1));
		instancesForCategory = model::InstancesForCategory(std::stoi(category_id), category_label);

		unsigned int idx, idx2;
		json_t *root, *images;
		json_error_t error;

		filepath = dir_path +filepath;

		/// Leggo il json tree del file da cui estrarre i dati
		root = json_load_file(filepath.c_str(), 0, &error);
		if(!root)
		{
			fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
			// return 1;
		}

		images = json_object_get(root, "images");

		/// Estraggo tutti i dati e li inserisco nell'oggetto che li deve contenere
		for(idx = 0; idx < json_array_size(images); idx++) {

			json_t *img, *image_id, *instance_ids;
			int uimage_id;
			std::string simage_id;

			// Prendo l'oggetto che contiene i dati per l'immagine
			img = json_array_get(images, idx);

			// Prendo l'id dell'immagine
			image_id = json_object_get(img, "image_id");
			//uimage_id = json_integer_value(image_id);

			simage_id = json_string_value(image_id);
			uimage_id = std::stoi(simage_id);

			// Prendo l'array degli id delle instance per la data categoria e immagine
			instance_ids = json_object_get(img, "instance_ids");

			for(idx2 = 0; idx2 < json_array_size(instance_ids); idx2++) {
				json_t *instance_id;
				int uinstance_id;
				std::string sinstance_id;

				instance_id = json_array_get(instance_ids, idx2);
				//uinstance_id = json_integer_value(instance_id);

				sinstance_id = json_string_value(instance_id);
				uinstance_id = std::stoi(sinstance_id);

				//printf("[buildInstancesForCategories] %d\n", uinstance_id);
				instancesForCategory.addInstanceForImage(uimage_id, uinstance_id);
			}

		}

		/// Inserisco l'oggetto che contiene tutti i dati per un data categoria all'interno dell'hashmap
		std::pair<int , model::InstancesForCategory> data(std::stoi(category_id), instancesForCategory);
		instancesForCategories->insert(data);

		json_decref(root);
	}

	closedir( dp );

}

} /* namespace controller */

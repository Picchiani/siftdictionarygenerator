/*
 * RapidJsonManager.h
 *
 *  Created on: Nov 12, 2014
 *      Author: coman
 */

#ifndef SRC_CONTROLLER_JANSSONMANAGER_H_
#define SRC_CONTROLLER_JANSSONMANAGER_H_

#include "../model/ImageData.h"
#include <jansson.h>
#include <vector>
#include <unordered_map>
#include <fstream>

namespace controller {

class JanssonManager {
public:
	JanssonManager();
	virtual ~JanssonManager();

	void parseInstancesFile(std::unordered_map<int, model::ImageData> *imagesData, std::string path, std::string type);

};

} /* namespace controller */

#endif /* SRC_CONTROLLER_JANSSONMANAGER_H_ */

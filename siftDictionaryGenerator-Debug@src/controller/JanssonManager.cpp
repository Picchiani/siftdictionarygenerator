/*
 * RapidJsonManager.cpp
 *
 *  Created on: Nov 12, 2014
 *      Author: coman
 */

#include "JanssonManager.h"

namespace controller {

JanssonManager::JanssonManager() {
	// TODO Auto-generated constructor stub

}

JanssonManager::~JanssonManager() {
	// TODO Auto-generated destructor stub
}
/**
 * Carica i dati delle immagini dai file json
 */
void JanssonManager::parseInstancesFile( std::unordered_map<int, model::ImageData> *imagesData, std::string path, std::string type) {
	unsigned int idx;

	json_t *root;
    json_error_t error;

    json_t *images;

    model::ImageData imageData;

    printf("Parsing file...\n");
    std::string file_path = path +"instances_" +type +"2014.json";

	//std::ifstream file(path +"instances_" +type +"2014.json", std::ifstream::binary);

	root = json_load_file(file_path.c_str(), 0, &error);

	if(!root)
	{
	    fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
	    // return 1;
	}

	// check that the returned value really is an array:
	//if(!json_is_array(root))
	//{
	//    fprintf(stderr, "error: root is not an array\n");
	//    json_decref(root);
	    //return 1;
	//}

	printf("Read images data...\n");
	images = json_object_get(root, "images");

	for(idx = 0; idx < json_array_size(images); idx++) {

		json_t *img, *image_id, *file_path, *file_name; //, *commit, *message;
		int uimage_id;
		std::string sfile_path;
		std::string sfile_name;
		//const char *message_text;

		img = json_array_get(images, idx);

		image_id = json_object_get(img, "id");
		uimage_id = json_integer_value(image_id);
		file_path = json_object_get(img, "file_path");
		sfile_path = std::string(json_string_value(file_path));
	    file_name = json_object_get(img, "file_name");
		sfile_name = std::string(json_string_value(file_name));

		imageData = model::ImageData(uimage_id, sfile_path, sfile_name);
		// imageData.printData();

		std::pair<int , model::ImageData> data(uimage_id, imageData);
		imagesData->insert(data);

	}

	json_decref(images);

	/*
	printf("Read instances...\n");
	unsigned int idx2, idx3;
    json_t *instances;

	instances = json_object_get(root, "instances");

	for(idx = 0; idx < json_array_size(instances); idx++) {
		model::Instance instanceObj;
		json_t *instance, *instance_id, *image_id, *category_id, *bbox, *segmentation;
		int uinstance_id;
		int uimage_id;
		int ucategory_id;
		int area;
		float y,x,fwidth, fheight;

		instance = json_array_get(instances, idx);

		instance_id = json_object_get(instance, "id");
		uinstance_id = json_integer_value(instance_id);
		image_id = json_object_get(instance, "image_id");
		uimage_id = json_integer_value(image_id);
		category_id = json_object_get(instance, "category_id");
		ucategory_id = json_integer_value(category_id);

		bbox = json_object_get(instance, "bbox");
		segmentation = json_object_get(instance, "segmentation");

		//segmentationSize = segmentation.size();
		std::vector< std::vector<cv::Point2f>> segments;

		for(idx2 = 0; idx2<json_array_size(segmentation); ++idx2) {
			json_t *segment;
			std::vector<cv::Point2f> contour;

			segment = json_array_get(segmentation, idx2);

			for(idx3 = 0; idx3<json_array_size(segment); ++idx3) {
				if(idx3 % 2) {
					y = json_number_value(json_array_get(segment, idx3));
					//cv::Point2f pt = cv::Point2f(x,y);
					//std::cout << "[parseInstancesFile] Point "<< pt.x << " " << pt.y << "\n";
					contour.push_back(cv::Point2f(x,y));
					//std::cout << "[parseInstancesFile] "<< x << " " << y << "\n";
				} else {
					x = json_number_value(json_array_get(segment, idx3));
				}
			}

			segments.push_back(contour);
		}

		x = 0; //bbox[0].asFloat();
		y = 0; //bbox[1].asFloat();
		fwidth = 0; // bbox[2].asFloat();
		fheight = 0; // bbox[3].asFloat();

		instanceObj = model::Instance(uinstance_id, uimage_id, ucategory_id, area, x, x, fwidth, fheight, segments);
		//instanceObj.printInstance();

		(imagesData->at(uimage_id)).addInstance(uinstance_id, instanceObj);

	}

	json_decref(instances);
*/
	json_decref(root);

	json_delete(root);

	printf("Finish parsing.\n");

}

} /* namespace controller */

/*
 * SIFTManager.cpp
 *
 *  Created on: Dec 15, 2014
 *      Author: coman
 */

#include "SIFTManager.h"

namespace controller {

SIFTManager::SIFTManager() {

}

SIFTManager::~SIFTManager() {
	// TODO Auto-generated destructor stub
}

/**
 * Imposta il path dove sono contenute le immagini
 * @param pathDataset path della cartella dove sono contenute le immagini
 */
void SIFTManager::setPathDataset(const std::string& pathDataset) {
	path_dataset = pathDataset;
}

/**
 * Imposta il path dove devono essere salvati i dizionari
 * @param pathDataset path della cartella dove devono essere salvati i dizionari
 */
void SIFTManager::setSaveDictionaryPath(const std::string& saveDictionaryPath) {
	this->saveDictionaryPath = saveDictionaryPath;
}



/**
 * Dato in in ingresso la lista totale delle immagini e la lista delle immagini per le categorie si calcola i descrittori SIFT non clusterizzati
 * per tutte le immagini.
 * @param imagesData lista di tutte le immagini del dataset COCO
 * @param instancesForCategories lista delle immagini per le categorie
 */
void SIFTManager::extractUnclusteredSiftFeatures(std::unordered_map<int, model::ImageData> *imagesData, std::set<int> image_ids) {

	/// input filename
	std::string filename;
	/// corrente immagine di input
	cv::Mat input;

	/// Immagazzina i keypoints che devono essere estratti da SIFT
	std::vector<cv::KeyPoint> keypoints;
	/// Immagazzina i descrittori SIFT della corrente immagine
	cv::Mat descriptor;

	/// Il SIFT feature extractor and descriptor
	cv::SiftDescriptorExtractor detector;

	printf("Extract Sift...\n");

	int count = 0;
	for(int image_id : image_ids) {
		filename = path_dataset +"/" +(*imagesData).at(image_id).getFileName();

		input = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE); /// Carica come grayscale
		/// Rileva i feature points
		detector.detect(input, keypoints);
		/// Calcola i descrittori per ogni  keypoint
		detector.compute(input, keypoints,descriptor);
		// Mette tutti i descrittori delle feature in un singolo Mat object
		featuresUnclustered.push_back(descriptor);

		/// Mostra quante immagini sono state processate
		count++;
		if((count % 2000) == 0) {
			printf("%d\n", count);
		}

		if((count % 20000) == 0) {
			break;
		}
	}

}

/**
 * Dato un insieme di feature SIFT non clusterizzate costruisce un cluster con k uguale a dictSize
 */
void SIFTManager::createBagOfSift(int size) {
	///Construct BOWKMeansTrainer

	//cv::Size featSize = featuresUnclustered.size();
	//printf("FeatSize: %d %d", featSize.width, featSize.height);

	/// Definisce i Term Criteria per l'algoritmo di clustering
	cv::TermCriteria tc(CV_TERMCRIT_ITER,100,0.001);
	/// Numero di volta cui si vuole riprovare
	int retries=1;
	/// Flag che indica la modalità
	int flags=cv::KMEANS_PP_CENTERS;
	/// Crea un BoW (o BoF) trainer
	cv::BOWKMeansTrainer bowTrainer(size,tc,retries,flags);
	/// Clusterizza i feature vectors
	cv::Mat dictionary = bowTrainer.cluster(featuresUnclustered);
	/// Salva il vocabolario trovato
	cv::FileStorage fs(saveDictionaryPath +"BagOfSift_" +std::to_string(size) +".yml", cv::FileStorage::WRITE);
	fs << "vocabulary" << dictionary;
	fs.release();
}

/**
 * Costruisce un dizionario di tipo BagOfSift dalle immagini contenute nella variabile instancesOfCategories

void SIFTManager::createBagOfSiftDictionary(std::unordered_map<int, model::ImageData> *imagesData, std::unordered_map<int, model::InstancesForCategory> *instancesForCategories, int dictionary_size) {
	/// Dalla unordered_map instancesForCategories mi prendo la lista delle immagini
	std::set<int> image_ids = createImageList(instancesForCategories);

	//Step 1 - Obtain the set of bags of features.

	//to store the input file names
	std::string filename;
	//to store the current input image
	cv::Mat input;

	//To store the keypoints that will be extracted by SIFT
	std::vector<cv::KeyPoint> keypoints;
	//To store the SIFT descriptor of current image
	cv::Mat descriptor;
	//To store all the descriptors that are extracted from all the images.
	cv::Mat featuresUnclustered;
	//The SIFT feature extractor and descriptor
	cv::SiftDescriptorExtractor detector;

	printf("Extract Sift...\n");

	int count = 0;
	for(int image_id : image_ids) {
		filename = path_dataset +"/" +(*imagesData).at(image_id).getFileName();

		//open the file
		input = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
		//detect feature points
		detector.detect(input, keypoints);
		//compute the descriptors for each keypoint
		detector.compute(input, keypoints,descriptor);
		//put the all feature descriptors in a single Mat object
		featuresUnclustered.push_back(descriptor);

		count++;
		if((count % 200) == 0) {
			printf("%d\n", count);
		}
	}

	printf("Construct dictionary...\n");

	//Construct BOWKMeansTrainer

	//define Term Criteria
	cv::TermCriteria tc(CV_TERMCRIT_ITER,100,0.001);
	//retries number
	int retries=1;
	//necessary flags
	int flags=cv::KMEANS_PP_CENTERS;
	//Create the BoW (or BoF) trainer
	cv::BOWKMeansTrainer bowTrainer(dictionary_size,tc,retries,flags);
	//cluster the feature vectors
	cv::Mat dictionary=bowTrainer.cluster(featuresUnclustered);
	//store the vocabulary
	cv::FileStorage fs("/media/coman/Tesi/Dictionary/BagOfSift_" +std::to_string(dictionary_size) +".yml", cv::FileStorage::WRITE);
	fs << "vocabulary" << dictionary;
	fs.release();
}
 */

} /* namespace controller */

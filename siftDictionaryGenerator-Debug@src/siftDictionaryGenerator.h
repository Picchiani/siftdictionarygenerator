/*
 * SIFTDICTIONARYGENERATOR_H_.h
 *
 *  Created on: Dec 1, 2014
 *      Author: Andrea Picchiani
 */

#ifndef SIFTDICTIONARYGENERATOR_H_
#define SIFTDICTIONARYGENERATOR_H_

#include <opencv2/opencv.hpp>
#include <array>
#include <string>

/// Path del dataset dove si trovano i file che descrivono lo split in train, val e test delle immagini
//std::string datasetInstancePath = "/media/coman/Tesi/datasetClassificatore/split/";
std::string datasetInstancePath = "/media/coman/Tesi/MicrosoftCOCO/COCOtrainSplit/";
/// Path dei file di split del train di MicrosoftCOCO sulla macchina in uni
//std::string datasetInstancePath = "/data_castore_2/picchiani/MicrosoftCOCO/COCOtrainSplit/

/// Root Path del dataset MicrosoftCOCO
const std::string rootCOCOPath = "/media/coman/Tesi/MicrosoftCOCO/";
/// Root Path del dataset MicrosoftCOCO sulla macchina in uni
//const std::string rootCOCOPath = "/data_castore_2/picchiani/MicrosoftCOCO/";
const std::string COCOAnnotationsPath = "annotations/";
const std::string COCOImagePath = "images/";

/// Path dove salvare i dizionari
const std::string saveDictionaryPath = "/media/coman/Tesi/MicrosoftCOCO/dictionarySplit/";
//const std::string saveDictionaryPath = "/data_castore_2/picchiani/MicrosoftCOCO/dictionarySplit/";


/// Variabile che indica il dataset usato
std::string type = "train";

/// Numero di visual word all'interno del dizionario
std::array<int, 1> dictSize = {200}; //, 500, 1000, 1500, 2000};

#endif /* SIFTDICTIONARYGENERATOR_H_ */
